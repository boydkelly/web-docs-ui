#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

export FORCE_SHOW_EDIT_PAGE_LINK="true"
export SOURCEMAPS=true
