async function fetchData() {
  const MAX_RETRIES = 3; // Maximum number of retry attempts
  let retries = 0;

  while (retries < MAX_RETRIES) {
    try {
      const response = await fetch('/_/proverbs.json'); // Replace with the JSON file URL

      if (!response.ok) {
        throw new Error(`Failed to fetch data (HTTP ${response.status})`);
      }

      const data = await response.json(); // Parse the JSON response

      if (!data || !Array.isArray(data) || data.length === 0) {
        throw new Error('Fetched data is not in the expected JSON format');
      }

      const randomIndex = Math.floor(Math.random() * data.length);
      const randomRecord = data[randomIndex];

      const dyu = randomRecord.dyu;
      const fr = randomRecord.fr;

      sessionStorage.setItem('dyu', dyu);
      sessionStorage.setItem('fr', fr);

      const processedWords = processSentence(dyu);
      console.log(dyu);
      console.log(processedWords);

      return; // Data successfully fetched, exit the loop
    } catch (error) {
      console.error('Error fetching data:', error);
      retries++;

      if (retries < MAX_RETRIES) {
        console.log(`Retrying (attempt ${retries})...`);
        // Add a delay before retrying to avoid rapid retries
        await new Promise(resolve => setTimeout(resolve, 1000));
      } else {
        throw error; // Maximum retries reached, propagate the error
      }
    }
  }
}

// Function to process a sentence
function processSentence(sentence) {
    // Function to generate a random number between min and max (inclusive)
    function getRandomSize(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Function to remove punctuation
    function removePunctuation(text) {
        return text.replace(/[^\w\s]/g, '');
    }
    // Remove punctuation from the sentence
    var cleanedSentence = removePunctuation(sentence);
    // Split the cleaned sentence into words
    var words = cleanedSentence.split(' ');
    // Create an array of objects with words and random sizes
    var myWords = words.map(function(word) {
        return {
            word: word,
            size: getRandomSize(10, 60) // Adjust the range as needed
        };
    });
    return myWords;
}

// Create a function to generate a word cloud
function createWordCloud(data) {
    var width = 800;
    var height = 400;

    var svg = d3.select('body').append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    var maxSize = d3.max(data, function(d) { return d.size; });

    var fontSizeScale = d3.scaleLinear()
        .domain([0, maxSize])
        .range([10, 60]);

    var colorScale = d3.scaleOrdinal(d3.schemeCategory10);

    var cloud = d3.layout.cloud()
        .size([width, height])
        .words(data)
        .padding(5)
        .rotate(function() { return (Math.random() - 0.5) * 30; })
        .fontSize(function(d) { return fontSizeScale(d.size); })
        .on('end', draw);

    cloud.start();

    function draw(words) {
        svg.selectAll('text')
            .data(words)
            .enter().append('text')
            .style('font-size', function(d) { return d.size + 'px'; })
            .style('fill', function(_, i) { return colorScale(i); })
            .attr('transform', function(d) {
                return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
            })
            .text(function(d) { return d.word; });

// Convert the SVG to a data URL
        var svgString = new XMLSerializer().serializeToString(svg.node());
        var dataURL = 'data:image/svg+xml;base64,' + btoa(svgString);

        // Set the background image of the div with class 'overview'
        d3.select('.overview-banner')
            .style('background-image', 'url(' + dataURL + ')');
    }
}

function updateUI() {
  const dyu = sessionStorage.getItem('dyu');
  const fr = sessionStorage.getItem('fr');

  document.getElementById('msg').innerHTML = dyu;
  document.getElementById('french').innerHTML = fr;

  // const target = `https://dyu.coastsystems.net:5005?page=0&neodash_proverb_text=${dyu}`;
  // document.querySelector('a#link_proverb').href = target;
}

async function checkNeodashAvailability() {
  try {
    await fetch('https://dyu.coastsystems.net:5005', { mode: 'no-cors' });
    console.log('Neodash is reachable');
  } catch (error) {
    document.querySelector('a#link_proverb').style.display = 'none';
    console.error('Neodash is not available:', error);
  }
}

