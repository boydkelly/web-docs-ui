let neoViz;

function renderWithRetry(config, cypherQuery, maxRetries) {
  let retries = 0;

  // Create a loading message element if it doesn't exist
  let loadingDiv = document.getElementById('loadingDiv');
  if (!loadingDiv) {
    loadingDiv = document.createElement('div');
    loadingDiv.id = 'loadingDiv';
    document.body.appendChild(loadingDiv);
  }
  loadingDiv.innerText = 'Loading graph...';

  async function attemptRender() {
    try {
      // Create a new NeoVis instance
      neoViz = new NeoVis.default(config);

      // Listen for the 'completed' event
      neoViz.registerOnEvent('completed', () => {
        console.log("Network rendered successfully");
//        neoViz.network.stabilize();
        // Remove the loading message
        loadingDiv.remove();
        console.log("Network stabilized");
      });

      // Render the graph with the provided Cypher query
      await neoViz.renderWithCypher(cypherQuery);
    } catch (error) {
      // Handle the error, you can log it or display an error message
      console.error("Error rendering network:", error);
      retries++;

      if (retries < maxRetries) {
        console.log(`Retrying (${retries}/${maxRetries})...`);
        setTimeout(attemptRender, 1000); // Retry after a delay (adjust the delay as needed)
      } else {
        loadingDiv.innerText = 'Failed to load the graph.';
        console.error("Max retries reached. Network rendering failed.");
      }
    }
  }

  attemptRender();
}

function draw() {
//  var serverUrl = 'neo4j://dyu.coastsystems.net:7687'
  var serverUrl = 'neo4j://1a53c4f7.databases.neo4j.io'
  const config = {
    containerId: 'viz',
    neo4j: {
      serverUrl: serverUrl,
      serverUser: 'neo4j',
      serverPassword: 'crash-sector-bassam',
      driverConfig: {
        encrypted: 'ENCRYPTION_ON',
        trust: 'TRUST_SYSTEM_CA_SIGNED_CERTIFICATES',
      },
    },
    visConfig: {
      layout: {
        improvedLayout: true,
        hierarchical: {
          enabled: false,
          sortMethod: 'directed',
          direction: 'UD'
        }
      },
      nodes: {
        shape: 'ellipse',
        font: {
          size: 28,
          color: 'black',
        },
      },
      edges: {
        arrows: {
          to: { enabled: true },
        },
        width: 2
      },
      physics: {
        enabled: true,
        maxVelocity: 50,
        solver: 'hierarchicalRepulsion',
        stabilization: {
          enabled: true,
          iterations: 300, // maximum number of iteration to stabilize
          fit: true
        },
        // wind: {
        //   x: 0.1, // Horizontal wind force
        //   y: 0.0, // Vertical wind force
        // },
        hierarchicalRepulsion: {
          nodeDistance: 120,
          springLength: 100
        },
        repulsion: {
          nodeDistance: 200,
          centralGravity: 0.2,
          springLength: 100
        },
        barnesHut: {
          gravitationalConstant: -2000,
          centralGravity: 0.3,
          springLength: 100,
          springConstant: 0.04,
          damping: 0.09,
          avoidOverlap: 1
          // damping: 0.3,
          // centralGravity: 0.7,
          // avoidOverlap: 0.9
        },
        forceAtlas2Based: {
          gravitationalConstant: -26,
          centralGravity: 0.2,
          springLength: 300,
          springConstant: 0.18,
          avoidOverlap: 1
        }
      }
    },
    labels: {
      Proverb: {
        fixed: true,
        label: 'text',
        [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
          static: {
            color: '#ee5396',
            shape: 'dot',
            size: 10,
            font: {
              background: 'none',
              strokeWidth: '0',
              size: 50,
              color: '#9f1853',
            },
          },
          function: {
            title: NeoVis.objectToTitleHtml,
          },
        },
      },
      Headword: {
        label: 'text',
        group: 'community',
        [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
          static: {
            color: '#3ddbd9',
            font: {
              background: 'none',
              strokeWidth: '0',
              color: 'black',
            },
          },
          function: {
            title: NeoVis.objectToTitleHtml,
          },
        },
      },
      Word: {
        label: 'text',
        [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
          static: {
            color: '#33b1ff',
            shape: 'box',
            font: {
              background: 'none',
              strokeWidth: '0',
              color: 'black',
            },
          },
          function: {
            title: NeoVis.objectToTitleHtml,
          },
        },
      },
    },
  };

  const proverb = sessionStorage.getItem('dyu');
  console.log(proverb);
  const cypher = 'MATCH p=()-[:MEANS]-(a:Headword)-[:USED_IN]-(d:Proverb) WHERE d.text ="' + proverb + '" RETURN p LIMIT 10';
//  const cypher = `MATCH (d:Proverb) WHERE d.lang = 'dyu' WITH d, rand() as r ORDER BY r LIMIT 1 MATCH e=(d)-[:MEANING]-() MATCH f=(d)-[:USED_IN]-(a:Headword)-[:MEANS]-() RETURN * LIMIT 50`

  renderWithRetry(config, cypher, 3); // Retry up to 3 times
}

console.log("network stabilized 1");

