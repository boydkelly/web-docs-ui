/* eslint-disable */
(function () {
  'use strict';
  var images = document.querySelectorAll('img');

  function hasParentClass(child, classname) {
    if (child.className.split(' ').indexOf(classname) >= 0) return true;
    try {
      //Throws TypeError if child doesn't have parent any more
      return child.parentNode && hasParentClass(child.parentNode, classname);
    } catch (TypeError) {
      return false;
    }
  }

  for (var i = 0; i < images.length; i++) {
    if (
      !(
        images[i].classList.contains('icon') ||
        images[i].classList.contains('copy-icon') ||
        images[i].classList.contains('copy-icon') ||
        images[i].classList.contains('pencil') ||
        hasParentClass(images[i], 'overview-box')
      )
    ) {
      images[i].setAttribute('data-zoomable', 'true');
    }
  }

  mediumZoom('[data-zoomable]');
})();
