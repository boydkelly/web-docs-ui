const modeToggle = document.getElementById('cycle-theme');
const el = document.firstElementChild
const prefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
const prefersLight = window.matchMedia('(prefers-color-scheme: light)').matches;
let preferredTheme = 'light'; // Default to light theme
if (prefersDark) {
  preferredTheme = 'dark';
} else if (prefersLight) {
  preferredTheme = 'light';
}

var savedTheme = window.localStorage.getItem('data-theme');

if(savedTheme) {
  el.setAttribute('color-scheme', savedTheme);
}
else {
  el.setAttribute('color-scheme', preferredTheme)
}

modeToggle.addEventListener('click', () => {

  const switcher = document.querySelector('#cycle-theme')
  const themes = ["light", "dark", "dim"];
  const activeTheme = el.getAttribute('color-scheme');
  // console.log(activeTheme)
  const activeIndex = themes.findIndex((val) => val === activeTheme);
  // const activeIndex = themes.findIndex((c) => el.getAttribute('color-scheme'));
  const nextIndex = (activeIndex + 1) % themes.length;
  const nextTheme = themes[nextIndex]
  // console.log(activeTheme)
  //
  // console.log(activeIndex)
  // console.log(nextTheme)
  el.setAttribute('color-scheme', nextTheme)
  window.localStorage.setItem('data-theme', nextTheme)
});
